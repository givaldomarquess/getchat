package android01.getinfo.net.br.android01.chat;

import java.util.ArrayList;

import android01.getinfo.net.br.android01.entity.Mensagem;

/**
 * Created by Givaldo Marques on 08/12/2017
 */

class ChatPresenterImpl implements ChatInterface.ChatPresenter, ChatInterface.ChatInteractor.OnNewMensagemListener {

    ArrayList<Mensagem> mensagens;
    private ChatInterface.ChatView chatView;
    private ChatInterface.ChatInteractor chatInteractor;

    public ChatPresenterImpl(ChatInterface.ChatView chatView, ChatInterface.ChatInteractor chatInteractor) {
        this.chatView = chatView;
        this.chatInteractor = chatInteractor;
        mensagens = Mensagem.getMensagensList(0);
        chatView.initChatAdapter(mensagens);
    }

    @Override
    public void doEnviarMensagem(Mensagem mensagem) {
        if (!mensagem.getConteudo().isEmpty() && !mensagem.getUsuario().isEmpty() && !mensagem.getHora().isEmpty()) {
            chatInteractor.enviarMensagem(mensagem);
            chatView.setMensagemErro(null);
        } else {
            chatView.setMensagemErro("Campo obrigatório");
        }
    }

    @Override
    public void doGetMensagens() {
        chatInteractor.getMensagens(this);
    }

    @Override
    public void onNewMensagem(Mensagem mensagem) {
        mensagens.add(mensagem);
        chatView.attChatAdapter(mensagens.size() - 1);
    }

    @Override
    public void mensagemModificada(Mensagem mensagem) {
        chatView.showNotification(mensagem);
        chatView.attChatAdapter(mensagens.size() - 1);

    }
}
