package android01.getinfo.net.br.android01.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import android01.getinfo.net.br.android01.R;
import android01.getinfo.net.br.android01.entity.Mensagem;

/**
 * Created by Givaldo Marques on 05/12/2017
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private ArrayList<Mensagem> mensagens;
    private Context context;

    private DatabaseReference mReference = FirebaseDatabase.getInstance().getReference();


    public ChatAdapter(ArrayList<Mensagem> mensagens, Context context) {
        this.mensagens = mensagens;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View mensagensView = inflater.inflate(R.layout.item_chat, parent, false);
        return new MyViewHolder(mensagensView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Mensagem m = mensagens.get(position);
        String c = m.getCurtidas() + "";

        holder.mensagemTextView.setText(m.getConteudo());
        holder.emailTextView.setText(m.getUsuario());
        holder.horaTextView.setText(m.getHora());

        holder.curtidasTextView.setText(c);

        holder.curtidasTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Mensagem curtida");

                m.setCurtidas(m.getCurtidas() + 1);

                mReference.child("mensagens").child(m.getId()).setValue(m);
            }
        });
    }

    private void showToast(String mensagem) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return mensagens.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView emailTextView;
        TextView mensagemTextView;
        TextView horaTextView;
        TextView curtidasTextView;

        public MyViewHolder(View itemView) {
            super(itemView);

            emailTextView = itemView.findViewById(R.id.emailTextView);
            mensagemTextView = itemView.findViewById(R.id.mensagemTextView);
            horaTextView = itemView.findViewById(R.id.horaTextView);
            curtidasTextView = itemView.findViewById(R.id.curtidasTextView);


        }


    }

}
