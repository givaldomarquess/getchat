package android01.getinfo.net.br.android01.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import android01.getinfo.net.br.android01.R;
import android01.getinfo.net.br.android01.chat.ChatActivity;
import android01.getinfo.net.br.android01.entity.Usuario;

public class MainActivity extends AppCompatActivity {

    String TAG = "MainActivity";

    private SharedPreferences sharedPreferences;
    private Usuario user;

    private EditText emailEditText;
    private EditText passwordEditText;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        loginButton = findViewById(R.id.loginButton);

        sharedPreferences = getPreferences(Context.MODE_PRIVATE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        user = Usuario.getUsuarioPreferencias(sharedPreferences);

        if (user != null) {
            emailEditText.setText(user.getEmail());
            passwordEditText.setText(user.getSenha());
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e = emailEditText.getText().toString();
                String p = passwordEditText.getText().toString();

                validForm(e, p);
            }
        });

        loginButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //showToast();
                return true;
            }
        });

    }

    private void validForm(String e, String p) {

        if (e.trim().isEmpty()) {
            emailEditText.setError("Campo obrigatório");

        } else if (p.trim().isEmpty()) {
            passwordEditText.setError("Campo obrigatório");

        } else {
            Usuario u = new Usuario(e, p);
            Usuario.salvarUsuarioPreferencias(sharedPreferences, u);
            startChatActivity();
        }

    }

    private void showToast(String e, String p) {
        Toast.makeText(this, "Email: " + e + "\nSenha: " + p, Toast.LENGTH_SHORT).show();
    }

    private void showToast() {
        Toast.makeText(this, "Longo clique", Toast.LENGTH_SHORT).show();
    }


    private void startChatActivity() {
        Intent i = new Intent(this, ChatActivity.class);

        Usuario usuario = new Usuario(emailEditText.getText().toString(),
                passwordEditText.getText().toString());

        i.putExtra("usuarioExtra", usuario);

        startActivity(i);
        finish();
    }




}
