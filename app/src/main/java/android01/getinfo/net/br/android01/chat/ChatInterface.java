package android01.getinfo.net.br.android01.chat;

import java.util.ArrayList;

import android01.getinfo.net.br.android01.entity.Mensagem;

/**
 * Created by Givaldo Marques on 08/12/2017
 */

interface ChatInterface {

    interface ChatInteractor {

        void enviarMensagem(Mensagem mensagem);

        void getMensagens(OnNewMensagemListener listener);

        interface OnNewMensagemListener {

            void onNewMensagem(Mensagem mensagem);

            void mensagemModificada(Mensagem mensagem);

        }

    }


    interface ChatPresenter {

        void doEnviarMensagem(Mensagem mensagem);

        void doGetMensagens();


    }


    interface ChatView {

        void initChatAdapter(ArrayList<Mensagem> mensagems);

        void attChatAdapter(int scrollPosicao);

        void setMensagemErro(String erro);

        void showNotification(Mensagem mensagem);

    }

}
