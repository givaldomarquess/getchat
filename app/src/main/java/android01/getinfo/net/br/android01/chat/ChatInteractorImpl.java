package android01.getinfo.net.br.android01.chat;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import android01.getinfo.net.br.android01.entity.Mensagem;

/**
 * Created by Givaldo Marques on 08/12/2017
 */

class ChatInteractorImpl implements ChatInterface.ChatInteractor {

    private static final String TAG = "ChatInteractorImpl";
    private final DatabaseReference mReference = FirebaseDatabase.getInstance().getReference();

    @Override
    public void enviarMensagem(Mensagem mensagem) {
        mReference.child("mensagens").child(System.currentTimeMillis() + "").setValue(mensagem);
    }

    @Override
    public void getMensagens(final OnNewMensagemListener listener) {
        mReference.child("mensagens").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Log.d(TAG, dataSnapshot.toString());
                Mensagem m = dataSnapshot.getValue(Mensagem.class);

                if (m != null) {
                    m.setId(dataSnapshot.getKey());
                }
                listener.onNewMensagem(m);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Mensagem m = dataSnapshot.getValue(Mensagem.class);

                listener.mensagemModificada(m);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
