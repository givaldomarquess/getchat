package android01.getinfo.net.br.android01.entity;

import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

/**
 * Created by Givaldo Marques on 05/12/2017
 */

public class Usuario implements Parcelable {

    public static final Creator<Usuario> CREATOR = new Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
    private String email;
    private String senha;

    public Usuario(String email, String senha) {
        this.email = email;
        this.senha = senha;
    }

    protected Usuario(Parcel in) {
        email = in.readString();
        senha = in.readString();
    }

    public static void salvarUsuarioPreferencias(@NonNull SharedPreferences sharedPreferences,
                                                 Usuario user) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userPreferencias", user.toString());
        editor.apply();

    }

    public static Usuario getUsuarioPreferencias(@NonNull SharedPreferences preferences) {
        return Usuario.toUsuario(preferences.getString("userPreferencias", null));
    }

    public static Usuario toUsuario(String jsonUsuario) {
        return new Gson().fromJson(jsonUsuario, Usuario.class);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(senha);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
