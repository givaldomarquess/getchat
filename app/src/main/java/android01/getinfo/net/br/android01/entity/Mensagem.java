package android01.getinfo.net.br.android01.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Givaldo Marques on 05/12/2017
 */

public class Mensagem implements Parcelable {

    public static final Creator<Mensagem> CREATOR = new Creator<Mensagem>() {
        @Override
        public Mensagem createFromParcel(Parcel in) {
            return new Mensagem(in);
        }

        @Override
        public Mensagem[] newArray(int size) {
            return new Mensagem[size];
        }
    };
    private String id;
    private String usuario;
    private String conteudo;
    private String hora;
    private int curtidas;

    public Mensagem() {

    }

    public Mensagem(String usuario, String conteudo) {
        this.usuario = usuario;
        this.conteudo = conteudo;
        hora = new SimpleDateFormat("HH:mm", Locale.US).format(new Date());
    }

    protected Mensagem(Parcel in) {
        id = in.readString();
        usuario = in.readString();
        conteudo = in.readString();
        hora = in.readString();
        curtidas = in.readInt();
    }

    public static ArrayList<Mensagem> getMensagensList(int qtd) {

        ArrayList<Mensagem> ms = new ArrayList<>();

        for (int i = 0; i < qtd; i++) {
            Mensagem m = new Mensagem("João", "mensagem de número: " + i);
            ms.add(m);
        }

        return ms;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(usuario);
        dest.writeString(conteudo);
        dest.writeString(hora);
        dest.writeInt(curtidas);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCurtidas() {
        return curtidas;
    }

    public void setCurtidas(int curtidas) {
        this.curtidas = curtidas;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

}
