package android01.getinfo.net.br.android01.chat;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import android01.getinfo.net.br.android01.R;
import android01.getinfo.net.br.android01.entity.Mensagem;
import android01.getinfo.net.br.android01.entity.Usuario;
import android01.getinfo.net.br.android01.main.MainActivity;

public class ChatActivity extends AppCompatActivity implements ChatInterface.ChatView {

    private static final String TAG = "ChatActivity";

    Usuario usuario;
    EditText mensagemEditText;
    ImageView enviarImageView;
    TextView emailTextView;

    Toolbar toolbar;

    RecyclerView recyclerView;
    ChatAdapter chatAdapter;

    ChatInterface.ChatPresenter chatPresenter = new ChatPresenterImpl(this, new ChatInteractorImpl());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        usuario = getIntent().getParcelableExtra("usuarioExtra");

        toolbar = findViewById(R.id.toolbar);

        mensagemEditText = findViewById(R.id.mensagemEditText);
        enviarImageView = findViewById(R.id.enviarImageView);
        emailTextView = findViewById(R.id.emailTextView);
        recyclerView = findViewById(R.id.chatRecyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(chatAdapter);

        setSupportActionBar(toolbar);
        toolbar.setTitle("GetChat");

    }

    @Override
    protected void onStart() {
        super.onStart();

        String s = usuario.getEmail();
        emailTextView.setText(s);

        enviarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = mensagemEditText.getText().toString();
                mensagemEditText.setText("");
                enviarMensagem(texto);

            }
        });

        chatPresenter.doGetMensagens();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.sairItem:
                //Iniciando a main activity
                startActivity(new Intent(this, MainActivity.class));
                return true;

            case R.id.sobreItem:
                finishAffinity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void showToast(Usuario usuario) {
        Toast.makeText(this, "email: " + usuario.getEmail() +
                "\nsenha: " + usuario.getSenha(), Toast.LENGTH_SHORT).show();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void enviarMensagem(String mensagem) {
        Mensagem m = new Mensagem(usuario.getEmail(), mensagem);
        chatPresenter.doEnviarMensagem(m);
    }


    @Override
    public void initChatAdapter(ArrayList<Mensagem> mensagems) {
        chatAdapter = new ChatAdapter(mensagems, this);

    }

    @Override
    public void attChatAdapter(int scrollPosicao) {
        chatAdapter.notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(scrollPosicao);
    }

    @Override
    public void setMensagemErro(String erro) {
        mensagemEditText.setError(erro);
    }

    @Override
    public void showNotification(Mensagem mensagem) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.getinfo_logo)
                        .setContentTitle(mensagem.getUsuario())
                        .setContentText(mensagem.getConteudo());
        Intent resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(0, mBuilder.build());
    }
}
